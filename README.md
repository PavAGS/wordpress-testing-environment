# Wordpress testing environment

## My docker compose file to building wp containers

Create file ```docker-compose.yml```

And paste this:


```
version: "3.3"

services:
  db:
    image: mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
volumes:
  db_data: {}
  ```
  Save the file and run this command:
  ```docker-compose up -d```

  Next step is edit the .htac
  Run  ```docker ps``` and check your wordpress container name

  Then run  ``` docker exec -ti container_name bash ```

Install nano
```apt-get update```

```apt-get install vim nano```

Open .htaccess file in nano

```nano .htaccess```

And paste this 

```php_value upload_max_filesize 64M
php_value post_max_size 64M
php_value max_execution_time 180
php_value max_input_time 180
php_value max_input_vars 5000
php_value upload_max_filesize 64M
php_value memory_limit 128M
```

